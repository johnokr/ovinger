# Paul skal kjøpe 3 bananer, 1 melk og 2 pakker havregryn.
# Han får 20% rabatt på havregryn.
# Han kjøper også en sjokolade til 30kr. Hva blir totalsummen?

banan_pris = 10.5
melk_pris =  23.5
havregryn_pris = 25
sjokolade_pris = 30

total_sum = 3*banan_pris + melk_pris + 2*havregryn_pris*0.8 + sjokolade_pris
print("Paul må betale ", total_sum, "kr")